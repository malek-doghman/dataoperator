#pragma once
#include "Foo.h"
class AnotherBar : public Foo {
public:
	explicit AnotherBar(int x) : Foo(x) {
		std::cout << "AnotherBar " << x;
	}
};
REGISTER_FOO(AnotherBar);
