#pragma once
#include <iostream>
#include "Registry.h"
class Foo
{
public:
	explicit Foo(int x);
	virtual ~Foo();
};

C10_DECLARE_REGISTRY(FooRegistry, Foo, int);
#define REGISTER_FOO(clsname) C10_REGISTER_CLASS(FooRegistry, clsname, clsname)
