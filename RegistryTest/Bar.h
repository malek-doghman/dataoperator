#pragma once
#include "Foo.h"
class Bar : public Foo {
public:
	explicit Bar(int x) : Foo(x) {
		std::cout << "Bar " << x;
	}
};
REGISTER_FOO(Bar);
