#include "Foo.h"

Foo::Foo(int x)
{
	std::cout << "Foo " << x;
}

Foo::~Foo()
{
}

C10_DEFINE_REGISTRY(FooRegistry, Foo, int);
