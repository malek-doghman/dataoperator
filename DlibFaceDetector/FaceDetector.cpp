#include "FaceDetector.h"

std::vector<dlib::matrix<dlib::rgb_pixel>> jitter_image(
	const dlib::matrix<dlib::rgb_pixel>& img) {
	// All this function does is make 100 copies of img, all slightly jittered by being
	// zoomed, rotated, and translated a little bit differently. They are also randomly
	// mirrored left to right.
	thread_local dlib::rand rnd;

	std::vector<matrix<rgb_pixel>> crops;
	for (int i = 0; i < 100; ++i)
		crops.push_back(jitter_image(img, rnd));

	return crops;
}

FaceDetector::FaceDetector()
{
	deserialize("data/mmod_human_face_detector.dat") >> detector;
}

FaceDetector::~FaceDetector()
{
}

void padRectangle(dlib::rectangle& rec, int pad_value) {
	rec.set_left(rec.left() - pad_value);
	rec.set_top(rec.top() - pad_value);
	rec.set_right(rec.left() + rec.width() + pad_value);
	rec.set_bottom(rec.top() + rec.height() + pad_value);
}

void autoPadRectangle(dlib::rectangle& rec, float percentage) {
	assert(percentage < 1. && percentage > 0.);
	int pad_value = rec.width() * percentage;
	padRectangle(rec, pad_value);
}

std::vector<dlib::matrix<dlib::rgb_pixel>> FaceDetector::Detect(dlib::matrix<rgb_pixel> img)
{
	std::vector<dlib::matrix<dlib::rgb_pixel>> faces;
	for (auto face : detector(img)) {
		matrix<rgb_pixel> face_chip;
		autoPadRectangle(face.rect, 0.5);
		extract_image_chip(img, face.rect, face_chip);
		faces.push_back(std::move(face_chip));
	}
	return faces;
}

int FaceDetector::Group(std::vector<dlib::matrix<dlib::rgb_pixel>> faces)
{

	if (faces.size() == 0)
	{
		std::cout << "No faces found in image!" << std::endl;
		return 1;
	}

	// This call asks the DNN to convert each face image in faces into a 128D vector.
	// In this 128D vector space, images from the same person will be close to each other
	// but vectors from different people will be far apart.  So we can use these vectors to
	// identify if a pair of images are from the same person or from different people.  
	std::vector<matrix<float, 0, 1>> face_descriptors = net(faces);


	// In particular, one simple thing we can do is face clustering.  This next bit of code
	// creates a graph of connected faces and then uses the Chinese whispers graph clustering
	// algorithm to identify how many people there are and which faces belong to whom.
	std::vector<sample_pair> edges;
	for (size_t i = 0; i < face_descriptors.size(); ++i)
	{
		for (size_t j = i; j < face_descriptors.size(); ++j)
		{
			// Faces are connected in the graph if they are close enough.  Here we check if
			// the distance between two face descriptors is less than 0.6, which is the
			// decision threshold the network was trained to use.  Although you can
			// certainly use any other threshold you find useful.
			if (length(face_descriptors[i] - face_descriptors[j]) < 0.6)
				edges.push_back(sample_pair(i, j));
		}
	}
	std::vector<unsigned long> labels;
	const auto num_clusters = chinese_whispers(edges, labels);
	// This will correctly indicate that there are 4 people in the image.
	std::cout << "number of people found in the image: " << num_clusters << std::endl;


	// Now let's display the face clustering results on the screen.  You will see that it
	// correctly grouped all the faces. 
	std::vector<image_window> win_clusters(num_clusters);
	for (size_t cluster_id = 0; cluster_id < num_clusters; ++cluster_id)
	{
		std::vector<matrix<rgb_pixel>> temp;
		for (size_t j = 0; j < labels.size(); ++j)
		{
			if (cluster_id == labels[j])
				temp.push_back(faces[j]);
		}
		win_clusters[cluster_id].set_title("face cluster " + cast_to_string(cluster_id));
		win_clusters[cluster_id].set_image(tile_images(temp));
	}

	// Finally, let's print one of the face descriptors to the screen.  
	std::cout << "face descriptor for one face: " << trans(face_descriptors[0]) << std::endl;

	// It should also be noted that face recognition accuracy can be improved if jittering
	// is used when creating face descriptors.  In particular, to get 99.38% on the LFW
	// benchmark you need to use the jitter_image() routine to compute the descriptors,
	// like so:
	matrix<float, 0, 1> face_descriptor = mean(mat(net(jitter_image(faces[0]))));
	std::cout << "jittered face descriptor for one face: " << trans(face_descriptor) << std::endl;
	// If you use the model without jittering, as we did when clustering the bald guys, it
	// gets an accuracy of 99.13% on the LFW benchmark.  So jittering makes the whole
	// procedure a little more accurate but makes face descriptor calculation slower.

	return 0;
}
