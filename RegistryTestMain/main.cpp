#include <iostream>
#include <memory>
#include "Foo.h"

int main(int argc, char **argv) {
	std::unique_ptr<Foo> bar(FooRegistry()->Create("Bar", 1));
	std::unique_ptr<Foo> another_bar(FooRegistry()->Create("AnotherBar", 1));
	return 0;
}