# README #

###DataOperator###

DataOperator is a C++ project used to process/prepare bunch of data for ML tasks.

###Dependencies###
- Dlib
- Opencv

I am using vcpkg to install any third-parties.

###Features###
- Registry (taken from caffe2 sources)
- Threadpool