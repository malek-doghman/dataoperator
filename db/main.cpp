#include "rocksdb/db.h"

int main() {
	rocksdb::DB* db;
	rocksdb::Options options;
	options.create_if_missing = true;
	options.error_if_exists = true;
	rocksdb::Status status =
		rocksdb::DB::Open(options, "/tmp/testdb", &db);
	assert(status.ok());
	std::string value;
	rocksdb::Status s = db->Get(rocksdb::ReadOptions(), "hello", &value);
	if (s.ok()) s = db->Put(rocksdb::WriteOptions(), "hello", value);
	s = db->Get(rocksdb::ReadOptions(), "hello", &value);
	if (s.ok()) s = db->Delete(rocksdb::WriteOptions(), "hello");
	delete db;
}
