#include <dlib/opencv.h>
#include <opencv2/opencv.hpp>
#include "FaceDetectorOp.h"
#include "DlibFaceDetector/FaceDetector.h"

FaceDetectorOp::FaceDetectorOp()
{
	face_detector_ = std::make_unique<FaceDetector>();
}

FaceDetectorOp::~FaceDetectorOp()
{
}

cv::Mat DlibMatToCvMat(const dlib::matrix<rgb_pixel>& dlib_img) {
	dlib::array2d<bgr_pixel> img_rgb;
	dlib::assign_image(img_rgb, dlib_img);
	auto mat = dlib::toMat(img_rgb);

	//A copy is needed here as dlib::toMat does not
	//a copy hence "mat" will depend upon the lifetime
	//of the object "img_rgb" that will be destroyed when 
	//the function returns.
	return mat.clone();
}

std::vector<cv::Mat> FaceDetectorOp::Process(const cv::Mat & image)
{
	dlib::matrix<rgb_pixel> dlib_img;
	dlib::assign_image(dlib_img, dlib::cv_image<dlib::bgr_pixel>(image));
	auto results = face_detector_->Detect(dlib_img);
	std::vector<cv::Mat> faces;
	std::transform(results.begin(), results.end(),
		std::back_inserter(faces), &DlibMatToCvMat);
	return faces;
}
