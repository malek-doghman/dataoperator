#pragma once
#include <memory>
#include "ImageOp.h"
class FaceDetector;
class FaceDetectorOp :
	public ImageOp
{
public:
	FaceDetectorOp();
	~FaceDetectorOp();
	std::vector<cv::Mat> Process(const cv::Mat& image);
	std::unique_ptr<FaceDetector> face_detector_;
};
REGISTER_IMAGE_OP(FaceDetectorOp);