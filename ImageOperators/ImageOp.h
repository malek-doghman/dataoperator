#pragma once
//Link with /WHOLEARCHIVE:$(SolutionDir)$(Platform)\$(Configuration)\<name>.lib
#include <opencv/cv.h>
#include "Registry/Registry.h"
class ImageOp
{
public:
	ImageOp() {};
	virtual ~ImageOp() {};
	virtual std::vector<cv::Mat> Process(const cv::Mat& image)=0;
};
C10_DECLARE_REGISTRY(ImageOpRegistry, ImageOp);
#define REGISTER_IMAGE_OP(clsname) C10_REGISTER_CLASS(ImageOpRegistry, clsname, clsname)
