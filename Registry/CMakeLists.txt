add_library(libregistry "")
set_target_properties(libregistry PROPERTIES LINKER_LANGUAGE CXX)

target_sources(libregistry
    PUBLIC
        "${CMAKE_CURRENT_LIST_DIR}/Export.h"
        "${CMAKE_CURRENT_LIST_DIR}/Macros.h"
        "${CMAKE_CURRENT_LIST_DIR}/Registry.h"
        "${CMAKE_CURRENT_LIST_DIR}/Type.cpp"
        "${CMAKE_CURRENT_LIST_DIR}/Type.h"       
)

target_include_directories (libregistry PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_compile_options(libregistry PUBLIC -std=c++17 -pedantic -Wall -Wextra)