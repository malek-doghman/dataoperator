#include <iostream>
#include <filesystem>
#include <vector>
#include <string>
#include <algorithm>

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

#include "ImageOperators/ImageOp.h"
#include "ThreadPool/thread_pool.h"
#include "ThreadPool/FutureTracker.h"

typedef std::chrono::high_resolution_clock::time_point TimeVar;
#define duration(a) std::chrono::duration_cast<std::chrono::milliseconds>(a).count()
#define timeNow() std::chrono::high_resolution_clock::now()

bool is_image(const std::filesystem::path& path) {
	std::vector<std::string> image_extensions{ ".png", ".jpg" };
	return std::find(std::begin(image_extensions), std::end(image_extensions),
		path.extension()) != std::end(image_extensions);
}

void do_work(std::string_view input_folder) {
	FutureTracker< std::vector<cv::Mat> > tracker;
	std::vector<std::filesystem::path> paths;
	size_t num_thread = std::ceil(ThreadPool::get_hardware_concurrency() * 0.1);
	std::cout << num_thread;
	ThreadPool pool(num_thread);
	for (std::filesystem::recursive_directory_iterator i(input_folder), end; i != end; ++i) {
		if (!std::filesystem::is_directory(i->path())) {
			if (is_image(i->path())) {
				paths.push_back(i->path());
				std::string file = i->path().string();
				tracker.EmplaceBack(pool.enqueue([file] {
					std::cout << " began work " << file << std::endl;
					cv::Mat image = cv::imread(file);
					auto face_detector = ImageOpRegistry()->Create("FaceDetectorOp");
					auto time_now = timeNow();
					auto faces = face_detector->Process(image);
					auto duration = duration(timeNow() - time_now);

					std::cout << duration;
					std::cout << " in the file " << file << std::endl;
					std::cout << "number of faces = "
						<< faces.size() << std::endl;

					return faces;
				}));
			}
		}
	}

	int total_number_of_faces = 0;
	while (true)
	{
		auto faces = tracker.WaitNextReady();
		if (faces)
		{
			total_number_of_faces += (*faces).size();
			std::for_each((*faces).begin(), (*faces).end(),
				[](const cv::Mat& face) {
				cv::namedWindow("Display window");
				cv::imshow("Display window", face);
				cv::waitKey(0);
			});
		}
		else {
			break;
		}

	}
	std::cout << "total number of faces = " << total_number_of_faces << std::endl;
}

int main(int argc, char** argv)
{
	if (argc < 2)
	{
		std::cout << "please set the <input_folder>";
		return 1;
	}
	auto time_now = timeNow();
	do_work(argv[1]);
	std::cout << duration(timeNow() - time_now);
	return 0;
}
