#pragma once
#include <future>
#include <optional>

template<typename R>
bool is_ready(std::future<R> const& f) {
	return f.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
}

enum class data_status {
	is_ready,
	is_being_prepared,
	has_been_treated
};

struct data_flag {
	data_status status = data_status::is_being_prepared;
	int index = 0;
};

template<typename R>
class FutureTracker
{
public:
	FutureTracker() {};
	~FutureTracker() {};

	void EmplaceBack(std::future<R> future) {
		futures.emplace_back(std::move(future));
	}

	std::optional<R> WaitNextReady() {
		while (!futures.empty())
		{
			std::cout << "waiting for any item" << std::endl;
			auto iter = std::find_if(std::begin(futures), std::end(futures),
				[](const std::future<R>& future) -> bool { return is_ready(future); });
			if (iter != std::end(futures))
			{
				std::cout << "found an item" << std::endl;
				R item = iter->get();
				futures.erase(iter);
				return item;
			}
		}
		return {};
	}
private:
	// good to insert at the end and to delete
	// elements in constant complexity
	std::list <std::future<R>> futures;
};
