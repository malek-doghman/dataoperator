#pragma once
#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>
#include <functional>
#include <stdexcept>
#include <iostream>

class ThreadPool {
public:
	ThreadPool(size_t);
	template<class F, class... Args>
	auto enqueue(F&& f, Args&&... args)
		->std::future<typename std::result_of<F(Args...)>::type>;
	static unsigned int get_hardware_concurrency();
	~ThreadPool();
private:
	// need to keep track of threads so we can join them
	std::vector< std::thread > workers;
	// the task queue
	std::queue< std::function<void()> > tasks;

	// synchronization
	std::mutex m_queue_mutex;
	std::condition_variable m_condition;
	bool m_stop = false;
};

// the constructor just launches some amount of workers
inline ThreadPool::ThreadPool(size_t threads) {
	assert(threads > 0);
	for (size_t i = 0; i < threads; ++i)
		workers.emplace_back(
			[this] {
		while (true) {
			std::function<void()> task;

			{
				std::unique_lock<std::mutex> lock(this->m_queue_mutex);
				this->m_condition.wait(lock,
					[this] { return this->m_stop || !this->tasks.empty(); });
				if (this->m_stop && this->tasks.empty())
					return;
				task = std::move(this->tasks.front());
				this->tasks.pop();
			}

			task();
		}
	}
	);
}

// add new work item to the pool
template<class F, class... Args>
auto ThreadPool::enqueue(F&& f, Args&&... args)
-> std::future<typename std::result_of<F(Args...)>::type> {
	using return_type = typename std::result_of<F(Args...)>::type;

	auto task = std::make_shared< std::packaged_task<return_type()> >(
		std::bind(std::forward<F>(f), std::forward<Args>(args)...)
		);

	std::future<return_type> res = task->get_future();
	{
		std::unique_lock<std::mutex> lock(m_queue_mutex);

		// don't allow enqueueing after stopping the pool
		if (m_stop)
			throw std::runtime_error("enqueue on stopped ThreadPool");

		tasks.emplace([task]() { (*task)(); });
	}
	m_condition.notify_one();
	return res;
}

unsigned int ThreadPool::get_hardware_concurrency() {
	return std::thread::hardware_concurrency();
}
// the destructor joins all threads
inline ThreadPool::~ThreadPool() {
	{
		std::unique_lock<std::mutex> lock(m_queue_mutex);
		m_stop = true;
	}
	m_condition.notify_all();
	for (std::thread &worker : workers)
		worker.join();
}